const callback3 = require('./../callback3.js');

function testCallback3(listID = 'qwsa221'){

    callback3(listID)
    
    .then((resolvedString)=>{
        console.log(resolvedString);
    })
    .catch((rejectedString)=>{
        console.log(rejectedString);
    });
}

//default call
testCallback3();

module.exports = testCallback3;

