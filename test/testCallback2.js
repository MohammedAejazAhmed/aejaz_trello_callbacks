
const callback2 = require('./../callback2.js');
const boardID = "def453ed";

function testCallback2 (boardID = 'def453ed'){

    callback2(boardID)
    
    .then((resolvedString)=>{
        console.log(resolvedString);
    })
    .catch((rejectedString)=>{
        console.log(rejectedString);
    });
}

//default call
testCallback2();

module.exports = testCallback2;
