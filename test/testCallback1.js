const callback1 = require('./../callback1.js');

function testCallback1 (boardID){

    callback1(boardID)

    .then((resolvedString)=>{
        console.log(resolvedString);
    })
    .catch((rejectedString)=>{
        console.log(rejectedString);
    });
}

//default call
testCallback1();

module.exports = testCallback1;
