const boards = require('./Data/boards');

module.exports = function (searchID) {

    return new Promise((resolve, reject) => {

        setTimeout(() => {
            
            //search if id is same as user passed ID

            if (boards['id'] === searchID) {

                const name = boards['name'];
                resolve(`Found ID \nName is ${name}.`);

            } else {
                reject(`No such ID exist.`);
            }
        }, 2 * 1000);
    });
}
