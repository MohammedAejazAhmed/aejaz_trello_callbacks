
const lists = require('./Data/lists.json');

module.exports = function (searchID) {

    return new Promise((resolve, reject)=>{	
        
        setTimeout(() => {            
                    
            const keyArray = Object.keys(lists);
            let check = false;            
            
            //check if any listID is same as user passed listID  

            keyArray.forEach((key)=>{

                if(key === searchID){
                    
                    resolve(lists[key]);
                    check = true;
                }
            });
            
            if(check == false){
                reject('No such ID Exist');
            }

	    }, 2 * 1000);
    });
}


