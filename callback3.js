
const cards = require('./Data/cards.json');

module.exports = function (searchID) {

    return new Promise((resolve, reject)=>{
	
        setTimeout(() => {    
            
            const keyArray = Object.keys(cards);            
            let check = true;
            
            //check if listID is present in cards array            

            keyArray.forEach((property)=>{

                if(property === searchID){
                    
                    resolve(cards[property]);
                    check = false;
                }
            });
            
            if(check){
                reject('No such ID exist');
           }
             
	    }, 2 * 1000);
    });
}

